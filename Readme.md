# Two Factor Authenticator-CLI

[TOC]

## Introduction

Two Factor Authenticator Apps are common. There are many of them around.
This one should easily use a QR-Code which was generated
by Google Authenticator App and generate the One-Time-Password

## Installation

If you want to have your secrets gpg encrypted you need to install `gnupg`
You need to have python3 installed

Then you need to create e Virtual-Environment:

```
python -m venv venv
```
Next you have to activate the venv and install the required packages:
```
. ./venv/bin/activate
pip install -r requirements.txt
```

## Run the script

### usage:
```commandline
python twofactor_authenticator.py [-h] [--qrcode] [--json] --inputfile INPUTFILE [--pgp] [--url]

options:
  -h, --help            show this help message and exit
  --qrcode, -q          secrets in a qrcode export of google authenticator
  --json, -j            secrets in json format
  --inputfile INPUTFILE, -i INPUTFILE
                        Filename of input file
  --pgp, -p             use gpg to decrypt
  --url, -u             secrets in oauthotp url
```

Example for a pgp encrypted qrcode image with the name `qrcode.jpg.gpg`

```commandline
python twofactor_authenticator.py -i qrcode.jpg.gpg -p -q
```
Sample output looks like:
```commandline
Google:xxx@xxx:731409
Amazon Web Services:root-account-mfa-device@xxxxx:812894
```
