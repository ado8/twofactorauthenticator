#!/usr/bin/env python
import argparse
import subprocess
import protobuf_generated_python.google_auth_pb2 as pb
import cv2
import json as js
import base64
import urllib.parse as urlparse
import numpy as np
import pyotp

def readqrcodeimage(inputfile):
    '''
    This function just reads the inputfile and decodes the qr code to the url
    :param inputfile: image in jpg,png,.. formats
    :return: url
    '''
    f = inputfile.read()
    image = np.asarray(bytearray(f))
    img = cv2.imdecode(image,0)
    detector = cv2.QRCodeDetector()
    data, bbox, straight_qrcode = detector.detectAndDecode(img)
    return data


def pgp_decrypt(inputfile):
    '''
    This routine decrypts the input file using gpg -d and returns the output as filehandle
    :param inputfile: gpg encrypted input file (can by an image, json or url-file)
    :return: filehandle to decrypted filecontent
    '''
    return subprocess.Popen(["gpg","-d"],stdin=inputfile,stdout=subprocess.PIPE).stdout

def json(inputfile):
    '''
    Just returns the json file as a dict
    :param inputfile: takes a input file handle and converts the json into a dict
    :return: dict { 'secretname' : 'secret', ...}
    '''
    return js.load(inputfile)

def url(inputfile=None,inputbytes=None):
    '''
    takes the input as file handle or a buffer of bytes
    :param inputfile: file handle
    :param inputbytes: buffer of bytes
    :return:
    '''
    if inputfile:
        filedata=inputfile.read()
    else:
        filedata=inputbytes.encode()
    query = urlparse.urlparse(filedata).query
    data=urlparse.parse_qs(query, strict_parsing=True)[b'data'][0]
    data_base64_fixed=data.replace(b' ',b'+')
    data=base64.b64decode(data_base64_fixed, validate=True)
    payload=pb.MigrationPayload()
    payload.ParseFromString(data)
    secrets={}
    for raw_otp in payload.otp_parameters:
        secret = str(base64.b32encode(raw_otp.secret), 'utf-8').replace('=', '')
        secrets[raw_otp.name]=secret
    return secrets

parser=argparse.ArgumentParser()
parser.add_argument("--qrcode","-q",action='store_true',help="secrets in a qrcode export of google authenticator")
parser.add_argument("--json","-j",action='store_true',help="secrets in json format")
parser.add_argument("--inputfile","-i",required=True,type=argparse.FileType("rb"),help="Filename of input file")
parser.add_argument("--pgp","-p",action='store_true',help="use gpg to decrypt")
parser.add_argument("--url","-u",action='store_true',help="secrets in oauthotp url")
parser.add_argument("--output","-o",action='store_true',help="output secrets as json")
args=parser.parse_args()

if args.pgp:
    inputfile=pgp_decrypt(args.inputfile)
else:
    inputfile=args.inputfile
if args.qrcode:
    secrets=url(inputbytes=readqrcodeimage(inputfile))
if args.json:
    secrets=js.load(inputfile)
if args.url:
    secrets=url(inputfile)


if __name__ == '__main__':
    if args.output:
        print(js.dumps(secrets))
    else:
        for name,secret in secrets.items():
            totp = pyotp.TOTP(secret)
            print("%s:%s"%(name,totp.now()))
