#!/usr/bin/env python


import wx
import sys, os


assertMode = wx.APP_ASSERT_DIALOG

#----------------------------------------------------------------------------

class Log:
    def WriteText(self, text):
        if text[-1:] == '\n':
            text = text[:-1]
        wx.LogMessage(text)
    write = WriteText


class RunDemoApp(wx.App):
    def __init__(self, name, module):
        self.name = name
        self.demoModule = module
        wx.App.__init__(self, redirect=False)


    def OnInit(self):
        wx.Log.SetActiveTarget(wx.LogStderr())

        self.SetAssertMode(assertMode)

        frame = wx.Frame(None, -1, self.name, pos=(50,50), size=(200,100),
                        style=wx.DEFAULT_FRAME_STYLE, name="Authenticator")
        frame.CreateStatusBar()
        menuBar = wx.MenuBar()

        frame.SetMenuBar(menuBar)
        frame.Show(True)
        frame.Bind(wx.EVT_CLOSE, self.OnCloseFrame)

        win = self.demoModule.runTest(frame, frame, Log())

        # a window will be returned if the demo does not create
        # its own top-level window
        if win:
            # so set the frame to a good size for showing stuff
            frame.SetSize((640, 480))
            win.SetFocus()
            win.Raise()
            self.window = win

        else:
            # It was probably a dialog or something that is already
            # gone, so we're done.
            frame.Destroy()
            return True

        self.SetTopWindow(frame)
        self.frame=frame
        # This sets the window to stay on top till closed
        actual_style=self.frame.GetWindowStyle()
        new_style = actual_style ^ wx.STAY_ON_TOP
        self.frame.SetWindowStyle(new_style)
        return True

    def OnExitApp(self, evt):
        self.frame.Close(True)


    def OnCloseFrame(self, evt):
        if hasattr(self, "window") and hasattr(self.window, "ShutdownDemo"):
            self.window.ShutdownDemo()
        evt.Skip()


#----------------------------------------------------------------------------


def main(argv):
    if len(argv) < 2:
        print("Please specify a demo module name on the command-line")
        raise SystemExit

    # ensure the CWD is the demo folder
    demoFolder = os.path.realpath(os.path.dirname(__file__))
    os.chdir(demoFolder)

    name, ext  = os.path.splitext(argv[1])
    module = __import__(name)
    app = RunDemoApp(name, module)
    app.MainLoop()



if __name__ == "__main__":
    main(sys.argv)


